Pod::Spec.new do |s|
    s.name              = 'PushwizeObjC'
    s.version           = '1.1.6'
    s.summary           = 'Pushwize iOS SDK'
    s.homepage          = 'https://mondriaan.com/'
    s.author            = { 'Levente Dimény' => 'levente.dimeny@mondriaan.com' }
    s.license           = 'Proprietary'
    s.platform          = :ios
    s.source            = { :http => "https://pods.sanomamdc.com/PushwizeObjC/PushwizeObjC-1.1.6.zip" }
    s.ios.deployment_target = '9.0'
    s.module_name = 'PushwizeObjC'

    s.vendored_frameworks = 'PushwizeObjC/PushwizeObjC.xcframework'
    s.dependency 'Pushwize', '1.1.6'

end
